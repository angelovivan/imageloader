package com.example.imageloader.network;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkManager {
    private static final String TAG = NetworkManager.class.getName();

    private static final int THREAD_POOL_SIZE = 3;

    private static NetworkManager sNetworkManager;
    private static Handler sHandler = new Handler(Looper.getMainLooper());
    private static Retrofit sRetrofit;
    private static OkHttpClient sOkHttpClient;

    private ScheduledThreadPoolExecutor mExecutor;

    private NetworkManager() {
        init();
    }

    public static synchronized NetworkManager getInstance() {
        if (sNetworkManager == null) {
            sNetworkManager = new NetworkManager();
        }

        return sNetworkManager;
    }

    private void init() {
        Log.d(TAG, "Init NetworkManager!");
        mExecutor = new ScheduledThreadPoolExecutor(THREAD_POOL_SIZE);

        initNetwork();
    }

    private static void initNetwork() {

        String baseUrl = "https://www.googleapis.com";
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder okHttpBuilder = new OkHttpClient.Builder();
        okHttpBuilder.addInterceptor(logging);
        sOkHttpClient = okHttpBuilder.build();

        sRetrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .callFactory(sOkHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public <T> T generateService(Class<T> clazz) {
        return sRetrofit.create(clazz);
    }

    public static Handler getHandler() {
        return sHandler;
    }

    public static Future<?> runInThreadPool(Runnable toRun) {
        return getInstance().mExecutor.submit(wrapRunnable(toRun));
    }

    private static Runnable wrapRunnable(final Runnable runnable) {
        return () -> {
            try {
                runnable.run();
            } catch (Exception ex) {
                Log.e(TAG, "Uncaught exception in a NETWORK thread!", ex);
            }
        };
    }
}
