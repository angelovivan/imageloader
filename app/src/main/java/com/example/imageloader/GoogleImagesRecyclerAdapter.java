package com.example.imageloader;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.example.imageloader.model.GoogleImage;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class GoogleImagesRecyclerAdapter extends RecyclerView.Adapter<GoogleImagesRecyclerAdapter.ViewHolder> {

    List<GoogleImage> mValues;
    Context mContext;
    protected ItemListener mListener;

    public GoogleImagesRecyclerAdapter(Context context, List<GoogleImage> values, ItemListener itemListener) {

        mValues = values;
        mContext = context;
        mListener = itemListener;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public ImageView imageView;
        public RelativeLayout relativeLayout;
        GoogleImage item;

        public ViewHolder(View v) {

            super(v);

            imageView = v.findViewById(R.id.imageView);
            relativeLayout = v.findViewById(R.id.relativeLayout);

        }

        public void setData(GoogleImage item) {
            this.item = item;

            Picasso.get().load(item.link).into(imageView);
            relativeLayout.setBackgroundColor((Color.GRAY));
        }


        @Override
        public void onClick(View view) {
            if (mListener != null) {
                mListener.onItemClick(item);
            }
        }
    }

    @Override
    public GoogleImagesRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.image_item_layout, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull GoogleImagesRecyclerAdapter.ViewHolder holder, int position) {
        String imageLink = mValues.get(position).link.replace("http://", "https://");
        Picasso.get().load(imageLink).into(holder.imageView);
        holder.relativeLayout.setBackgroundColor((Color.GRAY));
    }

    @Override
    public int getItemCount() {

        return mValues.size();
    }

    public interface ItemListener {
        void onItemClick(GoogleImage item);
    }
}

