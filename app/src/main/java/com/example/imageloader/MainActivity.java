package com.example.imageloader;

import android.os.Bundle;

import com.example.imageloader.api.GoogleImageSearchApi;
import com.example.imageloader.api.ImageLoaderResponseCallback;
import com.example.imageloader.model.ImageItemsModel;
import com.example.imageloader.network.NetworkManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();

    private RecyclerView imagesRecyclerView;
    private GoogleImagesRecyclerAdapter adapter;
    private EditText searchEditText;
    private GoogleImageSearchApi searchApi = NetworkManager.getInstance().generateService(GoogleImageSearchApi.class);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        imagesRecyclerView = findViewById(R.id.google_images_rv);
        imagesRecyclerView.setHasFixedSize(true);

        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        imagesRecyclerView.setLayoutManager(layoutManager);

        searchEditText = findViewById(R.id.search_edit_text);
        searchEditText.setOnKeyListener((v, keyCode, event) -> {
            // If the event is a key-down event on the "enter" button
            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                // Perform action on key press
                String query = searchEditText.getText().toString().trim();
                searchApi.searchImage(ImageLoaderApp.API_KEY_GOOGLE, ImageLoaderApp.CUSTOM_ENGINE_ID, query, "image")
                        .enqueue(new ImageLoaderResponseCallback<ImageItemsModel>() {

                    @Override
                    public void onSuccess(ImageItemsModel itemsModel) {
                        Log.d(TAG, "onSuccess() images: " + itemsModel);

                        adapter = new GoogleImagesRecyclerAdapter(getApplicationContext(), itemsModel.images, null);
                        imagesRecyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(String error) {
                        Log.w(TAG, "Network call returned error: " + error);
                    }
                });
                return true;
            }
            return false;
        });

        setSupportActionBar(toolbar);

//        FloatingActionButton fab = findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
