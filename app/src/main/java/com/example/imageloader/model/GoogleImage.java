package com.example.imageloader.model;

public class GoogleImage {

    public String kind;
    public String title;
    public String htmlTitle;
    public String link;
    public String displayLink;
    public String snippet;
    public String htmlSnippet;
    public String mime;


//
//    "kind": "customsearch#result",
//            2019-12-08 22:01:51.846 15541-15879/com.example.loadgoogleimages D/OkHttp:    "title": "Still Don't Get Bitcoin? Here's an Explanation For Five-Year-Olds",
//            2019-12-08 22:01:51.846 15541-15879/com.example.loadgoogleimages D/OkHttp:    "htmlTitle": "Still Don&#39;t Get Bitcoin? Here&#39;s an Explanation For Five-Year-Olds",
//            2019-12-08 22:01:51.846 15541-15879/com.example.loadgoogleimages D/OkHttp:    "link": "https://www.coindesk.com/wp-content/uploads/2014/01/apple-bitcoin.jpeg",
//            2019-12-08 22:01:51.846 15541-15879/com.example.loadgoogleimages D/OkHttp:    "displayLink": "www.coindesk.com",
//            2019-12-08 22:01:51.846 15541-15879/com.example.loadgoogleimages D/OkHttp:    "snippet": "Still Don't Get Bitcoin? Here's an Explanation For Five-Year-Olds",
//            2019-12-08 22:01:51.846 15541-15879/com.example.loadgoogleimages D/OkHttp:    "htmlSnippet": "Still Don&#39;t Get Bitcoin? Here&#39;s an Explanation For Five-Year-Olds",
//            2019-12-08 22:01:51.846 15541-15879/com.example.loadgoogleimages D/OkHttp:    "mime": "image/jpeg",
//            2019-12-08 22:01:51.846 15541-15879/com.example.loadgoogleimages D/OkHttp:    "image": {
//        2019-12-08 22:01:51.846 15541-15879/com.example.loadgoogleimages D/OkHttp:     "contextLink": "https://www.coindesk.com/bitcoin-explained-five-year-old",
//                2019-12-08 22:01:51.846 15541-15879/com.example.loadgoogleimages D/OkHttp:     "height": 1400,
//                2019-12-08 22:01:51.846 15541-15879/com.example.loadgoogleimages D/OkHttp:     "width": 1389,
//                2019-12-08 22:01:51.846 15541-15879/com.example.loadgoogleimages D/OkHttp:     "byteSize": 82217,
//                2019-12-08 22:01:51.846 15541-15879/com.example.loadgoogleimages D/OkHttp:     "thumbnailLink": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4DiYTLoLuS5B2sylMmdxY4PfH1i3QErZBXOR8EJcVqNkA863U5pmjWAhRVA&s",
//                2019-12-08 22:01:51.846 15541-15879/com.example.loadgoogleimages D/OkHttp:     "thumbnailHeight": 150,
//                2019-12-08 22:01:51.846 15541-15879/com.example.loadgoogleimages D/OkHttp:     "thumbnailWidth": 149
}
