package com.example.imageloader.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ImageItemsModel {

    @SerializedName("items")
    public List<GoogleImage> images;
}
