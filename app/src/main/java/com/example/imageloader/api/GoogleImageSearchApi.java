package com.example.imageloader.api;

import com.example.imageloader.model.ImageItemsModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GoogleImageSearchApi {

    @GET("/customsearch/v1")
    Call<ImageItemsModel> searchImage(@Query("key") String apiKey, @Query("cx") String cx, @Query("q") String query, @Query("searchType") String searchType);
}
