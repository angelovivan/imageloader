package com.example.imageloader.api;

import android.util.Log;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class ImageLoaderResponseCallback<T> implements Callback<T> {
    private static String TAG = ImageLoaderResponseCallback.class.getName();

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.isSuccessful()) {
            Log.d(TAG, "onResponse, response: " + response);
            onSuccess(response.body());
        } else {
            onError(response.errorBody().toString());
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        Log.w(TAG, "Network call failed: " + t);
    }

    public abstract void onSuccess(T response);

    public abstract void onError(String error);
}
